import { auth, database } from './firebase';
import React, { Component } from 'react';
import LinkSubmit from './components/link-submit';
import HandleUserLogin from './components/login';
import UserLinkList from './components/link-list';
import './static/css/app.css';

class App extends Component {
    constructor() {
        super();

        this.state = {
            user: null,
            userLinks: []
        };
    }

    componentDidUpdate(prevProps, prevState) {
        const { user, userLinks } = this.state;

        if (!user) {
            if (userLinks.length) this.setState({ userLinks: [] });
            return;
        }

        if (userLinks === prevState.userLinks) {
            const firebaseRefUserLinks = database.ref(`users/${auth.currentUser.uid}/links`);

            firebaseRefUserLinks.on('value', (data) => {
                const firebaseUserLinks = data.val();
                const userLinks = [];

                for (let link in firebaseUserLinks) {
                    userLinks.push({
                        id: link,
                        links: { ...firebaseUserLinks[link].links },
                        timeStamp: firebaseUserLinks[link].timeStamp
                    });
                }

                this.setState({ userLinks });
            });
        }
    }

    onHandleUserState = user => this.setState({ user });

    onHandleUserLinks = linksObject => {
        if (!this.state.user) return;

        const firebaseRefUser = database.ref(`users/${auth.currentUser.uid}/links`);
        firebaseRefUser.push({
            timeStamp: new Date().getTime(),
            links: linksObject
        });
    }

    onRemoveLink = id => {
        const firebaseRefUsersLinkId = database.ref(`users/${auth.currentUser.uid}/links/${id}`);
        firebaseRefUsersLinkId.remove();
    }

    render() {
        const { user, userLinks } = this.state;

        return (
            <div className="App">
                <HandleUserLogin handleUserState={this.onHandleUserState} user={user} />
                <LinkSubmit handleUserLinks={this.onHandleUserLinks} />
                {user && userLinks.length > 0 && (
                    <UserLinkList removeLink={this.onRemoveLink} userLinks={userLinks} />
                )}
            </div>
        );
    }
}

export default App;
