import firebase from 'firebase';

export const API_KEY = '';
export const FIREBASE_PROJECT_URL = '';

const config = {
    apiKey: API_KEY,
    authDomain: "",
    databaseURL: "",
    projectId: "",
    storageBucket: "",
    messagingSenderId: ""
};
firebase.initializeApp(config);

export const authProvider = new firebase.auth.GoogleAuthProvider();
export const auth = firebase.auth();
export const database = firebase.database();
export default firebase;