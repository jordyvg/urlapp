import React from 'react';
import LinkGenetrated from './link-generated';

const UserLinkList = ({ userLinks, removeLink }) =>  {
    const latestLinkFirst = userLinks.reverse();
    const renderLinks = latestLinkFirst.map(link => <LinkGenetrated key={link.timeStamp} link={link} removeLink={removeLink} />);

    return(
        <div>
            <h4>My links</h4>
            <ul className="link-list">
                {userLinks.length && renderLinks}
            </ul>
        </div>
    );
}

export default UserLinkList;