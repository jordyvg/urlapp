import { API_KEY, FIREBASE_PROJECT_URL } from '../firebase';
import React, { Component } from 'react';
import axios from 'axios';
import validUrl from 'valid-url';
import LinkGenetrated from './link-generated';

class LinkSubmit extends Component {
    constructor(props) {
        super(props);

        this.state = {
            submittedLink : '',
            longLink: '',
            shortLink: '',
            notice: '',
            loading: false,
            isSubmitable: false
        };
    }

    onInputChange(submittedLink) {
        let isSubmitable = false;
        if (submittedLink.length > 0) isSubmitable = true;

        this.setState({ submittedLink, isSubmitable });
    }

    onFormSubmit = (e) => {
        e.preventDefault();

        const { submittedLink, notice, isSubmitable } = this.state; 

        if (! isSubmitable) return;
        
        if (! validUrl.isUri(submittedLink)) {
            this.setState({ notice: 'Please enter a valid url: https://example.com' });
            return;
        }

        if (notice) this.setState({ notice: ''});

        const data = JSON.stringify({ 
            longDynamicLink: `${FIREBASE_PROJECT_URL}/?link=${submittedLink}`, 
            suffix: {
                option: "SHORT"
              }
        });

        axios.post(`https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=${API_KEY}`,
            data, { 
            headers: {
                'Content-Type': 'application/json',
            } }
        )
            .then(this.setState({ loading: true, isSubmitable: false }))
            .then(res => {
                const shortLink = res.data.shortLink;
                const longLink = submittedLink;

                this.props.handleUserLinks({ shortLink, longLink });
                this.setState({ 
                    submittedLink: '', 
                    longLink, 
                    shortLink, 
                    loading: false
                });
            })
            .catch(() => {
                alert('Something went wrong. Please check the XHR response.');
            });
    }

    render() {
        const { submittedLink, longLink, shortLink, notice, loading, isSubmitable } = this.state;
        const submitButtonClass = isSubmitable ? 'submit__button' : 'submit__button submit__button--inactive';
        const link = { links: { longLink, shortLink } };

        return (
            <div>
                <form className="submit" onSubmit={this.onFormSubmit}>
                    <input
                        className="submit__input"
                        placeholder="Shorten your link..." 
                        value={submittedLink}
                        onChange={e => this.onInputChange(e.target.value)}
                    />
                    <span className={loading ? 'is-loading' : null}>
                        <button className={submitButtonClass} type="submit">Shorten link</button>
                    </span>
                </form>
                {shortLink && ( 
                    <ul className="link-generated">
                        <LinkGenetrated link={link}/> 
                    </ul>
                )}
                {notice ? ( <p className="notice">{notice}</p> ) : null}
            </div>
        );
    }
}

export default LinkSubmit;