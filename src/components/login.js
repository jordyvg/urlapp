import { authProvider, auth, database } from '../firebase';
import React, { Component } from 'react';

class HandleUserLogin extends Component {
    componentDidMount() {
        auth.onAuthStateChanged(user => { 
            if (user) this.props.handleUserState(user);
        });
    }

    handleLogin = () => {
        auth.signInWithPopup(authProvider) 
            .then(res => {
                this.props.handleUserState(res.user);
                this.createFirebaseUser(res.user);
            });
    }

    handleLogout = () => {
        auth.signOut()
            .then(() => this.props.handleUserState(null));
    }

    createFirebaseUser = ({ uid, email, displayName }) => {
        database.ref(`users/${uid}`).update({
          email,
          displayName
        });
    }

    render() {
        const user = this.props.user;

        return (
            <div className="header">
                <h3>Hi {user ? user.displayName : 'Unknown' }, lets shorten some links!</h3>
                {user ? (
                    <button onClick={this.handleLogout}>Logout</button>
                ) : (
                    <button onClick={this.handleLogin}>Login with Google</button>
                )}
            </div>
        );
    }
}

export default HandleUserLogin;