import React from 'react';

const LinkGenetrated = ({ link, removeLink }) => {
    const { links, id } = link;

    return (
        <li className="link">
            <span className="link__long" title={links.longLink}>{links.longLink}</span>
            <span className="link__arrow">⟶</span> 
            <a className="link__short" href={links.shortLink} target="_blank">{links.shortLink}</a>
            {id && <span className="link__remove" onClick={() => removeLink(id)}></span>}
        </li>
    );
}

export default LinkGenetrated;